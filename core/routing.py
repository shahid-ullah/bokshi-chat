# core/routing.py
from django.conf.urls import url

from core import consumers

# from django.urls import path


websocket_urlpatterns = [
    url(r'^ws$', consumers.ChatConsumer),
    # path('ws/group/', consumers.GroupChatConsumer),
]
