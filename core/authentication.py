# core/authentication.py
import requests
from django.contrib.auth import get_user_model

User = get_user_model()
headers = {'api-version': '1', 'device-id': '1234567890', 'device-type': 'android'}
# payload = {'username': 200000002986, 'password': 'abc123'}


class NothiAuthenticationBackend(object):
    def authenticate(self, request, username=None, password=None):
        payload = {'username': username, 'password': password}

        try:
            res = requests.post(
                "https://dev.nothibs.tappware.com/api/login",
                data=payload,
                headers=headers,
            )
            if res.status_code == 200:
                content = res.json()
                if content['status'] == 'success':
                    username = content['data']['user']['username']
                    # breakpoint()
                    username_eng = str(content['data']['employee_info']['name_eng'])
                    username_bng = str(content['data']['employee_info']['name_bng'])
                    personal_email = str(content['data']['employee_info']['personal_email'])
                    personal_mobile = str(content['data']['employee_info']['personal_mobile'])
                    office_name_eng = str(content['data']['office_info'][0]['office_name_en'])
                    office_name_bng = str(content['data']['office_info'][0]['office_name_bn'])
                    designation = str(content['data']['office_info'][0]['designation'])
                    nid = str(content['data']['employee_info']['nid'])
                    date_of_birth = str(content['data']['employee_info']['date_of_birth'])
                    active = str(content['data']['user']['active'])
                    father_name_eng = str(content['data']['employee_info']['father_name_eng'])
                    father_name_bng = str(content['data']['employee_info']['father_name_bng'])
            else:
                return None
        except:
            return None

        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            user = User.objects.create(username=username, password='',
                    username_eng=username_eng,
                    personal_email=personal_email,
                    personal_mobile=personal_mobile,
                    office_name_eng=office_name_eng,
                    nid=nid,
                    date_of_birth=date_of_birth,
                    active=active,
                    father_name_eng=father_name_eng,
                    )
            user.save()
        return user

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
