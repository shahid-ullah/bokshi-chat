let currentRecipient = '';
let chatInput = $('#chat-input');
let chatButton = $('#btn-send');
let userList = $('#user-list');
let messageList = $('#message-list');
let messageBox = $('#message-box');
let visibleInput=$('input-visibility')
// let contactProfile = $("#contact_profile")
let contactProfile = document.getElementById('receiver')
let userProfile = document.getElementById('user-profile')
let searchInput = $('#search-input')

// Fetch all users from database through api
function onSelectUser(user, username_eng){
  setCurrentRecipient(user, username_eng)
}

function updateUserList() {
  $.getJSON('api/v1/members/', function (data) {
    // userList.children('.user').remove();[mashuq commented this]
    // console.log(data)

    for (let i = 0; i < data.length; i++) {
      // const userItem = `<li class="contact">${data[i]['username']}</li>`;

      const userItem =
        `
            <div id="selectUser"   onclick="onSelectUser('${data[i]['username']}', '${data[i]['username_eng']}')">
            <a href="#" class="list-group-item list-group-item-action list-group-item-light rounded-0">
            <div class="media"><img src="https://randomuser.me/api/portraits/women/86.jpg" alt="Avatar" alt="user" width="50" class="rounded-circle">
              <div class="media-body ml-4">
                <div class="d-flex align-items-center justify-content-between mb-1">
                  <h6 class="mt-3 text-capitalize">${data[i]['username_eng']}</h6><small class="small font-weight-bold"></small>
                </div>
                <p class="font-italic text-muted mb-0 text-small"></p>
              </div>
            </div>
          </a>
          </div>
          `
      $(userItem).appendTo('#user-list');
    }
  });
}

// Receive one message and append it to message list
function drawMessage(message) {

  let date = new Date(message.timestamp);
  const hour=date.getHours()
  const minute=date.toLocaleString('en-US', { hour: 'numeric', hour12: true })
  const second=date.getSeconds()
  const day=date.getDay()
  const month=date.toLocaleString('default', { month: 'long' })


  // console.log(hour)

  // var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
  // date=date.toLocaleDateString("en-US")

  // console.log(date.toLocaleDateString("en-US")); // 9/17/2016
  // console.log(today.toLocaleDateString("en-US", options)); // Saturday, September 17, 2016
  // console.log(today.toLocaleDateString("hi-IN", options));

  if (message.user === currentUser) {
    const messageItem=`   <div class="message media w-50 ml-auto mb-3">
         <div class="media-body">
           <div class="bg-primary rounded py-2 px-3 mb-2">
             <p class="text-small mb-0 text-white">${message.body}</p>
           </div>
           <p class="small text-muted">${hour}:${minute} | ${month} ${day}  </p>
         </div>
       </div>

     </div>`
    $(messageItem).appendTo('#message-list');
  }
  else{
    const messageItem=` <div class="message media w-50 mb-3"><img src="https://randomuser.me/api/portraits/women/86.jpg" alt="Avatar" alt="user" width="50" class="rounded-circle">
         <div class="media-body ml-3">
           <div class="bg-light rounded py-2 px-3 mb-2">
             <p class="text-small mb-0 text-muted">${message.body}</p>
           </div>
           <p class="small text-muted">${hour}:${minute} | ${month} ${day} </p>
         </div>
       </div>`
    $(messageItem).appendTo('#message-list');
  }


  // $(messageItem).appendTo('#message-list');
  // console.log(currentUser)

}

// Fetch last 20 conversatio from the database
function getConversation(recipient) {
  $.getJSON(`/api/v1/message/?target=${recipient}`, function (data) {
    messageList.children('.message').remove();
    for (let i = data['results'].length - 1; i >= 0; i--) {
      drawMessage(data['results'][i]);
    }

  });

}

// Retrive message by message id and add to messageList
// Access message id from websocket
function getMessageById(message) {
  id = JSON.parse(message).message
  $.getJSON(`/api/v1/message/${id}/`, function (data) {
    if (data.user === currentRecipient ||
      (data.recipient === currentRecipient && data.user == currentUser)) {
      drawMessage(data);
    }
    ;
  });
}

// Send message to messages api
function sendMessage(recipient, body) {
  $.post('/api/v1/message/', {
    recipient: recipient,
    body: body
  }).fail(function () {
    alert('Error! Check console!');
  });

  userList.children('.user').remove()
}

// set clicked user as currentRecipient
// get all conversation of currentRecipient or currentUser
function setCurrentRecipient(username, username_eng) {
  // console.log(contactProfile);

  username_tag = contactProfile.getElementsByTagName('h6')[0]
  // console.log(b[0].innerText);
  username_tag.innerText = username_eng
  currentRecipient = username;
  // console.log(username);
  getConversation(currentRecipient);
  enableInput();
}


// Enable input button
function enableInput() {
  chatInput.prop('disabled', false);
  chatButton.prop('disabled', false);
  chatInput.focus();
  // chatInput.show()
  // chatButton.show()
}

// Disable input button
function disableInput() {
  chatInput.prop('disabled', true);
  chatButton.prop('disabled', true);
  // chatInput.hide()
  // chatButton.hide()

}

$(document).ready(function () {
  updateUserList();
  disableInput();
  // $("#message-box").scrollTop($(document).height());
  $("#search-box").hide()
  $("#back-button").hide()

  userProfile.getElementsByTagName('h6')[0].innerText=username_eng;
  //    let socket = new WebSocket(`ws://127.0.0.1:8000/?session_key=${sessionKey}`);
  var socket = new WebSocket(
    'ws://' + window.location.host +
    '/ws?session_key=${sessionKey}')
  // HTTP GET /api/v1/message/?target=test2 200 [0.87, 127.0.0.1:38868]
  chatInput.keypress(function (e) {
    // console.log(chatInput.val());
    if (e.keyCode == 13)
      chatButton.click();
  });
  searchInput.keypress(function(e){
    if(e.keyCode==13){
      $("#search-box").children(".update-list").remove()
      if(searchInput.val().length>0){
        drawSearchedUser(searchInput.val())
        searchInput.val('')
      }
    }
  })
  searchInput.click(function(e){
    // console.log("the search box is clicked!!!")
    $("#user-list").hide()
    $("#search-box").show()
    $("#back-button").show()

  })

  $("#back-button").click(function(e){
    // console.log("the imput is blured")
    $("#user-list").show()
    $("#search-box").hide()
    $("#back-button").hide()
    $("#search-box").children(".update-list").remove()
  })

  chatButton.click(function () {
    if (chatInput.val().length > 0) {
      // console.log((currentRecipient));
      sendMessage(currentRecipient, chatInput.val());
      chatInput.val('');
    }
  });

  // Receive message from websocket
  socket.onmessage = function (e) {
    getMessageById(e.data);
  };
});
function onSelectSearchedUser(selctedSearchedUser,selctedSearchedUserID, username_eng){
  $.post('/api/v1/member/add/', {
    creator: currentUserID,
    friends: selctedSearchedUserID
  }).fail(function () {
    alert('Error! Check console!');
  });

  setCurrentRecipient(selctedSearchedUser, username_eng)

  $("#user-list").show()
  $("#search-box").hide()
  $("#back-button").hide()
  $("#search-box").children(".update-list").remove()
  searchInput.val('')

}


function drawSearchedUser(user){
  $.getJSON(`/api/v1/usersearch/?search_user=${user}`, function (data) {
    for(let i=0;i<=data.length;i++)
    {
      let user=String(data[i]["username_eng"])
      const searchedUser=`<a href="#" class="list-group-item list-group-item-action update-list" onclick=onSelectSearchedUser('${data[i]["username"]}','${data[i]["id"]}','${encodeURIComponent(user)}')>${data[i]["username_eng"]}</a>`
      $(searchedUser).appendTo('#search-box');
    }

    // console.log(data)

  });
  // console.log(user)



}
