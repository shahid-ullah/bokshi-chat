# bokshi-chat
Required Libraries:

1. Install Redis
2. Install Mysql
3. Install pipenv

mysql config and redis connection config file chat/settings.py

apt install python3

apt install python3-pip

apt-get install python3-dev default-libmysqlclient-dev build-essential

apt install pipenv

pipenv install mysqlclient

pip3 install mysqlclient




Usage:
```sh
1. $ pipenv install
```
```sh
2. $ pipenv shell
```
```sh
3. $ python manage.py migrate
```
```sh
4. $ python manage.py collectstatic
```
```sh
5. $ python manage.py runserver
```
